JIRA Subversion Plugin
=======================
JIRA's Subversion integration lets you see Subversion commit information relevant to each JIRA issue.

This plugin is no longer being actively developed, and there are no plans to make it compatible with JIRA 7.4 or above.
If you'd like to work on the plugin, feel free to fork this repository.

Documentation
-------------
https://ecosystem.atlassian.net/wiki/display/SVN/JIRA+Subversion+plugin

Marketplace
-----------
Binary version of this plugin are available on Marketplace:
https://marketplace.atlassian.com/plugins/com.atlassian.jira.plugin.ext.subversion/server/overview

Support
-----------
Plugin is unsupported by Atlassian
